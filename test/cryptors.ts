/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

const cryptors = require('../src/cryptors');
import * as chai from 'chai';
const expect = chai.expect;
const crypto = require('crypto');

describe("Cryptors", function () {
    it("Encrypts and decrypts a buffer", () => {
        const b = crypto.randomBytes(100);
        const key = crypto.randomBytes(32);
        const encrypted = cryptors.aesEncrypt(b, key);
        const decrypted = cryptors.aesDecrypt(encrypted, key);
        expect(b.toString()).to.equal(decrypted.toString());
    })

});
