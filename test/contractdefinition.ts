/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import 'mocha';
import * as chai from 'chai';
import * as fs from "mz/fs";
const expect = chai.expect;

import {ContractDefinition} from "../src/contractdefinition";
import {StandardTypeResolver} from "../src/objects";

describe("ContractDefinition", function () {
    this.timeout(50000);

    it("load", () => {
        return fs.readFile("test/futuristic.r4o").then((data) => {
            const cd = ContractDefinition.fromData(data, StandardTypeResolver);
            expect(cd.fieldInfo.length).to.be.above(0);
        })
    })

});