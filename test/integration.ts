/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import 'mocha';
import * as chai from 'chai';
import * as fs from "mz/fs";
const expect = chai.expect;

import {ContractDefinition} from "../src/contractdefinition";
import {MyEngine} from '../src/engine';
import MemoryConsensusEngine from '../src/consensus/memory';
import {ECDSACryptoSystem} from '../src/ecdsa';
import {makeSimpleSigner} from "../src/types";
import ContractInstance from "../src/contractinstance";
import {GTXMessageCodec} from "../src/gtxmessages";
import PostchainGTXConsensus from "../src/consensus/postchain";
import {boxSimpleValue, StandardTypeResolver} from "../src/objects";
import {encodeParameters} from "../src";

const crypto = require('crypto');
const cryptors = require('../src/cryptors');
const cryptoSystem = new ECDSACryptoSystem();

const testBlockchainRID = Buffer.from("78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3", "hex");

async function runIntegrationTest (consensusEngine, blockchainRID?) {
    if (!blockchainRID) blockchainRID = testBlockchainRID;
    console.log("blockchainRID:", blockchainRID);

    let keyPair = cryptoSystem.makeKeyPair();
    let instance: ContractInstance;

    const data = await fs.readFile("test/futuristic.r4o");

    const codec = new GTXMessageCodec(blockchainRID);
    const engine = new MyEngine(codec, cryptors.makeAESCryptor);
    const sessionKey = crypto.randomBytes(32);
    const cd = engine.registerContractDefinition(data);
    expect(cd.fieldInfo.length).to.be.above(0);

    const cryptorGen = (nonce) => engine.makeCryptor(sessionKey);

    instance = await engine.makeNewContractInstance(cd, [boxSimpleValue(keyPair.pubKey)], consensusEngine, cryptorGen);
    expect(instance).to.exist;
    await instance.performAction("OFFER",
        encodeParameters({"PROPERTY-ID": "frobla"},
            instance.contractDefinition.actions["OFFER"].parameters,
            instance.contractDefinition.typeResolver
        ),
        [keyPair.pubKey],
        makeSimpleSigner(cryptoSystem, keyPair)
    );
    console.log(instance.currentState.fields);
    let failed = false;
    try {
        await instance.performAction("OLALA", [], [keyPair.pubKey], makeSimpleSigner(cryptoSystem, keyPair));
    } catch (e) {
        failed = true;
    }
    expect(failed).to.be.true;

    const instance2 = await engine.loadContractInstance(instance.chainID, consensusEngine, cryptorGen(null));
    expect(instance2).to.exist;
}

describe("Integration", function () {
    this.timeout(50000);

    it("memory", function () {
        return runIntegrationTest(new MemoryConsensusEngine());
    });

    it("postchain-local", function () {
        if (! ("MANUAL" in process.env)) {
            this.skip();
            return;
        }
        const postchainConsensus = new PostchainGTXConsensus("http://localhost:7740", testBlockchainRID);
        return runIntegrationTest(postchainConsensus);
    });

    it("postchain-remote", function () {
        if (! ("POSTCHAIN_API" in process.env)) {
            this.skip();
            return;
        }
        let blockchainRID;
        if ('BLOCKCHAINRID' in process.env)
            blockchainRID = Buffer.from(process.env.BLOCKCHAINRID!, 'hex');
        else
            blockchainRID = testBlockchainRID;
        const postchainConsensus = new PostchainGTXConsensus(process.env.POSTCHAIN_API, blockchainRID);
        return runIntegrationTest(postchainConsensus, blockchainRID);
    });

});
