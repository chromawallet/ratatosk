/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {
    isBuffer, RType, PValue, TypeResolver, isString, isAssoc, isArray, PValueAssoc, PValueArray
} from './objects';
import {ECDSACryptoSystem} from './ecdsa';
import {deserializePValue} from './serialization';

const cryptoSystem = new ECDSACryptoSystem();

function requireString(s: any): string {
    if (isString(s)) return s;
    else throw Error("String expected");
}

function requireArray(a: any): PValue[] {
    if (isArray(a)) return a;
    else throw Error("Array expected");
}

export class ParameterInfo {
    name: string;
    description?: string;
    type: RType;
    constructor (tr: TypeResolver, def: PValue) {
        if (!isAssoc(def)) throw Error("Definition expected");
        this.name = requireString(def['NAME']);
        this.description = (def['DESCR']) ? requireString(def['DESCR']) : undefined;
        this.type = tr.getType(def['TYPE']);
    }
}

export class FieldInfo extends ParameterInfo {
    init?: PValue;
    constructor (tr: TypeResolver, def: PValue) {
        super(tr, def);
        if (!isAssoc(def)) throw Error("Definition expected");
        if ("INIT" in def) {
            this.init = def["INIT"];
        } else {
            this.init = undefined;
        }
    }
}

export class ActionGuard {
    clauses: PValue[];
    signatures: string[];
    constructor(def: PValue) {
        if (!isArray(def)) throw Error("Array required");
        this.clauses = [];
        this.signatures = [];
        for (const clause of def) {
            if (!isArray(clause)) throw Error("guard clause must be a list");
            const head = clause[0];
            if ("SIG" === requireString(head)) {
                this.signatures = requireArray(clause[1]).map(requireString);
            } else {
                this.clauses.push(clause);
            }
        }
    }
}

export interface UpdateEntry {
    name: string;
    expr: PValue;
}

export class ActionUpdate {
    entries: UpdateEntry[];
    constructor(def: PValue) {
        if (!isArray(def)) throw Error("Array required");
        this.entries = def.map( e => {
            if (!isArray(e)) throw Error("array expected");
            return {
                name: requireString(e[0]),
                expr: e[1]
            };
        });
    }
}

export class ActionInfo {
    name: string;
    description?: string;
    parameters: ParameterInfo[];
    parameterNames: string[];
    guard: ActionGuard;
    update: ActionUpdate;
    annotations: PValueArray;

    constructor (tr: TypeResolver, def: PValue) {
        if (!isAssoc(def)) throw Error("Definition expected");
        this.name = requireString(def["NAME"]);
        this.description = ("DESCR" in def) ? requireString(def['DESCR']) : undefined;
        if ("ANTS" in def) {
            this.annotations = requireArray(def["ANTS"]);
        }

        this.parameters = (requireArray(def["PARAMS"])).map(
            pdef => new ParameterInfo(tr, pdef)
        );
        this.parameterNames = this.parameters.map(p => p.name);
        this.guard = new ActionGuard(def["GUARD"]);
        this.update = new ActionUpdate(def["UPDATE"]);
    }
}

export class ContractDefinition {
    contractHash: string;
    fieldInfo: FieldInfo[];
    fieldNames: string[];
    fieldIndex: { [name: string]: number } = {};
    actions: { [name:string]: ActionInfo};
    name: string;
    typeResolver: TypeResolver;

    constructor(contractHash: string, def: PValue, tr: TypeResolver) {
        if (!isAssoc(def)) throw Error("Assoc requried");
        this.typeResolver = tr;
        this.contractHash = contractHash;

        this.name = requireString(def["NAME"]);

        this.fieldInfo = (requireArray(def['FIELDS'])).map( fdef => {
            return new FieldInfo(tr, fdef);
        });

        this.fieldNames = [];

        for (let i = 0; i < this.fieldInfo.length; ++i) {
            const field = this.fieldInfo[i];
            this.fieldNames.push(field.name);
            this.fieldIndex[field.name] = i;
        }

        this.actions = {};
        requireArray(def["ACTIONS"]).map( adef => {
            const action = new ActionInfo(tr, adef);
            this.actions[action.name] = action;
        });
    }

    static fromData(data: Buffer, tr: TypeResolver) {
        return new this(cryptoSystem.digest(data).toString('hex'), deserializePValue(data), tr);
    }
}

