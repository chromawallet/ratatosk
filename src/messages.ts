/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {
    MessageCodec, LogicalMessage, ConcreteMessage, RawMessage, PubKey, Signature, MessageFrame,
    Decryptor, Signer, Encryptor, MessageProposal
} from './types';
import {ECDSACryptoSystem} from './ecdsa';
import {PValue} from "./objects";
import {deserializePValue, serializePValue} from "./serialization";

const cryptoSystem = new ECDSACryptoSystem();

export class MyLogicalMessage implements LogicalMessage {
    action: string;
    params: PValue[];
    signedBy: PubKey[];
    constructor (action: string, params: PValue[], signedBy: PubKey[]) {
        this.action = action;
        this.params = params;
        this.signedBy = signedBy;
    }

    static from(lm: LogicalMessage) {
        return new this(lm.action, lm.params, lm.signedBy);
    }

    static decode(from: Buffer, signedBy: PubKey[]) {
        const data = deserializePValue(from) as Array<PValue>;
        return new this(
            data[0] as string,
            data[1] as PValue[],
            signedBy
        )
    }

    encode(): Buffer {
        const buffer =  serializePValue(
            [this.action, this.params]
        );
        console.log(buffer);
        return buffer;
    }
}

