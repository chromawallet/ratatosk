/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {
    Engine, MessageCodec, Decryptor, RawMessage, LogicalMessage, MessageFrame, Signer, PubKey,
    Encryptor, ConsensusEngine, Cryptor, CryptoSystem, KeyPair, makeSimpleSigner, MessageProposal, CryptorGen
} from './types';

import ContractInstance from './contractinstance';
import {ContractState} from "./contractstate";
import {ContractDefinition} from "./contractdefinition";
import {PValue, isBuffer, StandardTypeResolver, TValue} from "./objects";
import {ECDSACryptoSystem} from "./ecdsa";
import * as cryptors from "./cryptors";
import {GTXMessageCodec} from "./gtxmessages";
import {isFunction} from "util";

function identityB(b: Buffer): Buffer {  return b; }

const identityCryptor = {
    encryptor: identityB,
    decryptor: identityB
};

export class MyEngine implements Engine {
    codec: MessageCodec;
    contractDefinitions: {[id:string]:ContractDefinition};
    cryptoSystem: CryptoSystem;
    cryptorMaker?: (key:Buffer) => Cryptor;

    constructor(codec: MessageCodec, cryptorMaker?: (key:Buffer) => Cryptor) {
        this.codec = codec;
        this.cryptorMaker = cryptorMaker;
        this.contractDefinitions = {};
        this.cryptoSystem = new ECDSACryptoSystem();

        if (this.cryptorMaker === undefined){
          this.cryptorMaker = cryptors.makeAESCryptor;
        }
    }

    registerContractDefinition(data) {
        const def = ContractDefinition.fromData(data, StandardTypeResolver);
        this.contractDefinitions[def.contractHash] = def;
        return def;
    }

    encodeMessageProposal(chainID: string, rm: RawMessage): Buffer {
        return this.codec.encodeMessageProposal(chainID, rm);
    }
    decodeMessageProposal(m: Buffer): MessageProposal {
        return this.codec.decodeMessageProposal(m);
    }

    decodeMessage(rm: RawMessage, decryptor?: Decryptor): LogicalMessage {
        return this.codec.decodeMessage(rm, decryptor);
    }
    encodeMessage(msg: LogicalMessage, prevMessageFrame: MessageFrame, encryptor?: Encryptor): RawMessage {
        return this.codec.encodeMessage(msg, prevMessageFrame, encryptor);
    }
    sign(rm: RawMessage, pubkeys: PubKey[], signer: Signer): Promise<RawMessage> {
        return this.codec.sign(rm, pubkeys, signer);
    }

    makeCryptor(key: Buffer | null): Cryptor {
        if (key === null) {
            return identityCryptor;
        } else if (this.cryptorMaker !== undefined){
          return this.cryptorMaker(key);
        } else{
          throw Error('makeCryptor: this.cryptorMaker is not initialized');
        }
    }

    makeSimpleSigner(keypair: KeyPair): Signer {
        return makeSimpleSigner(this.cryptoSystem, keypair);
    }

    makeNewContractInstance(def: ContractDefinition, params: TValue[], ce: ConsensusEngine, cryptorGen: Cryptor | CryptorGen): Promise<ContractInstance> {
        const contractHash = def.contractHash;
        if (contractHash === undefined) return Promise.reject(Error("bad contract definition"));
        const eParams = params.map( p => p.toPrimitive());
        const encodedMessage: LogicalMessage = {
            action: contractHash,
            params: eParams,
            signedBy: []
        };
        return ce.getInitMessageFrame().then(
            frame => {
                let initState = ContractState.nullState(def);
                const initLM = {
                    // note that applied message is different from encoded message
                    action: "$INIT",
                    params: eParams,
                    signedBy: []
                };
                initState = initState.applyAction(initLM);
                console.log('Init state', initState);
                const cryptor = (cryptorGen instanceof Function) ?  cryptorGen(Buffer.from(frame.getID(), 'hex')) : cryptorGen;
                const rm = this.encodeMessage(encodedMessage, frame, cryptor.encryptor);
                return ce.postMessage(null, rm).then( () => {
                    return new ContractInstance(rm.getID(), this, rm, initLM, initState, ce, cryptor);
                });
            }
        )
    }

    loadContractInstance(chainID: string, ce: ConsensusEngine, cryptor: Cryptor): Promise<ContractInstance> {
        return ce.getNextMessage(chainID, null).then(
            (rm: RawMessage) => {
                const lm = this.decodeMessage(rm, cryptor.decryptor);
                const contractHash = lm.action;
                const def:ContractDefinition = this.contractDefinitions[contractHash];
                if (!def) throw Error("Contract definition not found");
                let initState = ContractState.nullState(def);
                lm.action = "$INIT";
                initState = initState.applyAction(lm);
                return new ContractInstance(chainID, this, rm, lm, initState, ce, cryptor);
            }
        )
    }


}
