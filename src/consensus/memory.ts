/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {ConsensusEngine, MessageResult, RawMessage, MessageFrame} from "../types";

class NonceMessageFrame implements MessageFrame {
    type = "nonce";
    id: string;

    constructor(cereal: number) {
        this.id = Buffer.from(cereal.toString()).toString('hex');
    }

    getID(): string {
        return this.id;
    }

    getPrevID(): string {
        return this.id;
    }
}

class Chain {
    messages: RawMessage[];

    constructor(initMessage) {
        this.messages = [initMessage];
    }

    getNextMessage(msgID: string | null): MessageResult {

        if (msgID === null) {
            return this.messages[0];
        } else {
            for (const m of this.messages) {
                if (m.getPrevID() === msgID) return m;
            }
            if (this.messages[this.messages.length - 1].getID() === msgID)
                return null;
            else
                return "error";
        }
    }

    getTransactionOrder(txRID: string): number | "error" {
        for (let message of this.messages) {
            if (message.getID() === txRID) {
                return this.messages.indexOf(message)
            }
        }
        return "error";
    }

    postMessage(m:  RawMessage) {
        this.messages.push(m);
    }

}

export default class MemoryConsensusEngine implements ConsensusEngine {

    private chains: { [name: string]: Chain } = {};
    private nChains: number = 0;

    getNextMessage(chainID: string, msgID: string | null): Promise<MessageResult> {
        if (!(chainID in this.chains)) return Promise.reject(Error("chain not found"));
        return Promise.resolve(this.chains[chainID].getNextMessage(msgID));
    };

    getBlockInfoByMessageID(chainID: string, messageID: string): Promise<any> {
        return Promise.resolve(this.chains[chainID].getTransactionOrder(messageID))
    }

    postMessage(chainID: string | null, m: RawMessage): Promise<void> {
        if (chainID === null) {
            this.chains[m.getID()] = new Chain(m);
            return Promise.resolve();
        }

        try {
            if (!(chainID in this.chains)) return Promise.reject(Error("chain not found"));
            this.chains[chainID].postMessage(m);
            return Promise.resolve();
        } catch (e) {
            return Promise.reject(e);
        }
    }

    getInitMessageFrame(): Promise<MessageFrame> {
        this.nChains++;
        return Promise.resolve(new NonceMessageFrame(this.nChains));
    }

}