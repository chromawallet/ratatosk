/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {gtxClient, restClient, gtx} from 'postchain-client';
import {ConsensusEngine, MessageFrame, MessageResult, RawMessage} from "../types";
import {cryptoSystem, GTXGenericMessage, GTXNonceMessageFrame, R4GTXRawMessage} from "../gtxmessages";
import {isArray} from "util";

const crypto = require('crypto');

export default class PostchainGTXConsensus implements ConsensusEngine {
    gtxClient: any;
    restClient: any;
    blockchainRID: Buffer;

    constructor(url, blockchainRID) {
        this.blockchainRID = blockchainRID;
        this.restClient = restClient.createRestClient(url);
        this.gtxClient = gtxClient.createClient(this.restClient,
            blockchainRID,
            ["R4createChain", "R4postMessage"]
        );
    }

    getNextMessage(chainID: string, msgID: string | null): Promise<MessageResult> {
        return new Promise<MessageResult>((resolve, reject) => {
            this.gtxClient.query({
                type: 'R4getMessages',
                chainID: chainID,
                sinceMessageID: msgID,
                maxHits: 1
            }, (err, res) => {
                if (err) return reject(err);
                if (!isArray(res)) return reject(Error("got bad data from Postchain"));
                if (res.length === 0) return resolve(null);
                const msg = res[0];
                if (msg.opIndexes.length !== 1)
                    return reject(Error("This client supports only single R4 call per GTX message"));
                try {
                    const gm = new GTXGenericMessage(gtx.deserialize(Buffer.from(msg.txData, 'hex')));
                    resolve(R4GTXRawMessage.fromGenericMessage(gm, msg.opIndexes[0]));
                } catch (e) {
                    reject(e);
                }
            });
        })
    }

    // getBlockTimeByTxRID(TxRID: String) chainId?
    _getBlockInfoByTxRID(chainID: string, txRID: string): Promise<any> {
        return new Promise<MessageResult>( (resolve, reject) => {
            this.gtxClient.query({
                type: "tx_confirmation_time",
                txRID: txRID
            }, (err, res) => {
                if (err) return reject(err);
                resolve(res)
            })
        });
    }

    getBlockInfoByMessageID(chainID: string, messageID: string): Promise<any> {
        return new Promise<MessageResult>((resolve, reject) => {
            this.gtxClient.query({
                type: "R4getTxRID",
                messageID: messageID
            }, (err, res) => {
                if (err) return reject(err);
                this._getBlockInfoByTxRID(chainID, res.txRID).then((info) => {
                    resolve(info);
                });
            })
        })
    }

    _waitConfirmation(txid: Buffer): Promise<void> {
        console.log("Polling ", txid.toString('hex'));
        return new Promise<void>((resolve, reject) => {
            setTimeout(() => {
                console.log("Polling ", txid.toString('hex'));
                this.restClient.status(txid, (err, res) => {
                    console.log("Got status response:", err, res);
                    if (err) reject(err);
                    else {
                        const {status} = res;
                        switch (status) {
                            case "confirmed":
                                resolve();
                                break;
                            case "rejected":
                                reject(Error("Message was rejected"));
                                break;
                            case "unknown":
                                reject(Error("Server lost our message"));
                                break;
                            case "waiting":
                                this._waitConfirmation(txid).then(resolve, reject);
                                break;
                            default:
                                console.log(status);
                                reject(Error("got unexpected response from server"));
                        }
                    }
                });
            }, 511);
        });
    }

    postMessage(chainID: string | null, m: RawMessage): Promise<void> {
        if (!(m instanceof R4GTXRawMessage))
            return Promise.reject(Error("wrong message type"));
        const gtxm: R4GTXRawMessage = <R4GTXRawMessage>m;

        if (!gtxm.genericMessage.isFullySigned(true))
            return Promise.reject("Message is not fully signed");

        const messageBuffer = gtxm.encode();
        const txRID = gtxm.getTxRID();

        return new Promise<void>((resolve, reject) => {
            try {
                this.restClient.postTransaction(messageBuffer, (err, res) => {
                    if (err) reject(err);
                    else resolve();
                });
            } catch (e) {
                reject(e);
            }
        }).then(() => {
            return this._waitConfirmation(txRID);
        });
    }

    getInitMessageFrame(): Promise<MessageFrame> {
        const nonce = cryptoSystem.digest(crypto.randomBytes(32));
        return Promise.resolve(new GTXNonceMessageFrame(nonce.toString('hex')));
    }

}