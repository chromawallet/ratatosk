/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {
    PValue, TValue, isString, NullTValue, isArray, boxSimpleValue, RType, CONST_TRUE,
    PrimitiveTypes, PrimitiveRType, PrimitiveTValue, CONST_FALSE, ArrayRType, TypeResolver, ArrayTValue
} from './objects';

export interface NamedOperator {
    isSpecial: boolean;
    name?: string;
}

export interface NormalOperator extends NamedOperator {
    isSpecial: false;
    apply(args: TValue[], env: EvalEnv): TValue;
}

export interface SpecialOperator extends NamedOperator {
    isSpecial: true;
    apply(args: PValue[], env: EvalEnv): TValue;
}

export type Operator = SpecialOperator | NormalOperator;

export interface EvalEnv {
    getValue(identifier: string): TValue;
    getOperator(identifier: string): Operator;
    getType(identifier: PValue): RType;
}

export function evalExpr (expr: PValue, env: EvalEnv): TValue {
    if (isString(expr)) {
        return env.getValue(expr);
    } else {
        if (expr === null) return NullTValue;
        if (isArray(expr)) {
            if (expr.length === 0) throw Error("Invalid call");
            const head = expr[0];
            if (!isString(head)) throw Error("Invalid call");
            const op = env.getOperator(head);
            const args = expr.slice(1);
            if (op.isSpecial) {
                return op.apply(args, env);
            } else {
                return op.apply( args.map( v => evalExpr(v, env) ), env )
            }
        } else {
            throw Error("Invalid expression");
        }
    }
}

const OP_QUOTE: SpecialOperator = {
    name: "QUOTE",
    isSpecial: true,
    apply: function (args: PValue[], env: EvalEnv): TValue {
        if (args.length === 1)
            return boxSimpleValue(args[0]);
        else if (args.length === 2) {
            const type = env.getType(args[1]);
            return type.fromPrimitive(args[0])
        }
        else throw Error("Wrong number of arguments to QUOTE");
    }
};

export function toBoolean(arg: TValue): boolean {
    if (arg.type === PrimitiveTypes.BOOLEAN) {
        return (arg as PrimitiveTValue).pv === 1
    }
    else throw Error("Not a boolean");
}

const OP_OR: NormalOperator = {
    name: "OR",
    isSpecial: false,
    apply: function (args: TValue[], env: EvalEnv): TValue {
        for (const arg of args) {
            if (toBoolean(arg)) return  CONST_TRUE;
        }
        return CONST_FALSE;
    }
};

const OP_AND: NormalOperator = {
    name: "AND",
    isSpecial: false,
    apply: function (args: TValue[], env: EvalEnv): TValue {
        for (const arg of args) {
            if (!toBoolean(arg)) return  CONST_FALSE;
        }
        return CONST_TRUE;
    }
};


export function requireNArgs(args: TValue[], count: number): void {
    if (args.length !== count) {
        throw Error(`Wrong number of arguments: ${count.toString()} provided, ${args.length.toString()} expected`);
    }
}

export const OP_EQL : NormalOperator = {
    name: "EQL",
    isSpecial: false,
    apply: function (args: TValue[], env?: EvalEnv): TValue {
        requireNArgs(args, 2);
        return args[0].equals(args[1]) ? CONST_TRUE : CONST_FALSE;
    }
};

const OP_NOT : NormalOperator = {
    name: "NOT",
    isSpecial: false,
    apply(args: TValue[]): TValue {
        requireNArgs(args, 1);
        const arg = args[0];
        return toBoolean(arg) ? CONST_FALSE : CONST_TRUE;
    }
};

export function requireTypedArgs(type: RType, args: TValue[]): void {
    for (const arg of args) {
        if (arg.type !== type) throw Error(`Type ${type.name} is requied`);
    }
}

const OP_PLUS: NormalOperator = {
    name: "+",
    isSpecial: false,
    apply(args: TValue[]): TValue {
        requireTypedArgs(PrimitiveTypes.INTEGER, args);
        if (args.length < 2) throw Error("Wrong number of arguments");
        let sum = 0;
        for (const arg of args)
            sum += (arg as PrimitiveTValue).pv as number;
        return PrimitiveTypes.INTEGER.fromPrimitive(sum);
    }
};

const OP_MINUS: NormalOperator = {
    name: "-",
    isSpecial: false,
    apply(args: TValue[]): TValue {
        requireTypedArgs(PrimitiveTypes.INTEGER, args);
        requireNArgs(args, 2);
        const arg1 = (args[0] as PrimitiveTValue).pv as number;
        const arg2 = (args[1] as PrimitiveTValue).pv as number;
        return PrimitiveTypes.INTEGER.fromPrimitive(arg1 - arg2);
    }
};

const OP_PUSH: NormalOperator = {
    name: "PUSH",
    isSpecial: false,
    apply(args: TValue[]): TValue {
        requireNArgs(args, 2);
        const what = args[0];
        const where = args[1];
        if (!(where instanceof ArrayTValue)) throw Error("Target must be an array");
        return where.push(what);
    }
};

const OP_IF: SpecialOperator = {
    name: "IF",
    isSpecial: true,
    apply(args: PValue[], env: EvalEnv): TValue {
        if (args.length !== 3) throw Error("Wrong IF structure");
        const condition = evalExpr(args[0], env);
        if (toBoolean(condition)) {
            return evalExpr(args[1], env);
        } else {
            return evalExpr(args[2], env);
        }
    }
};

function comparisonOperator(name, fn: (l: number, r:number)=>boolean): NormalOperator {
    return {
        name,
        isSpecial: false,
        apply(args: TValue[], env: EvalEnv): TValue {
            requireTypedArgs(PrimitiveTypes.INTEGER, args);
            requireNArgs(args, 2);
            const arg1 = (args[0] as PrimitiveTValue).pv as number;
            const arg2 = (args[1] as PrimitiveTValue).pv as number;
            return fn(arg1, arg2) ? CONST_TRUE : CONST_FALSE;
        }
    }
}

const OP_EQLS = comparisonOperator("=", (l, r) => l === r);
const OP_LESS = comparisonOperator("<", (l, r) => l < r);
const OP_LEQ = comparisonOperator("<=", (l, r) => l <= r);
const OP_GREATER = comparisonOperator(">", (l, r) => l > r);
const OP_GEQ = comparisonOperator(">=", (l, r) => l >= r);

export class BaseEvalEnv implements EvalEnv {
    valueBindings: { [name: string]: TValue } = {};
    opBindings: { [name: string]: Operator } = {};
    typeResolver?: TypeResolver;
    parent: EvalEnv | null;

    constructor (parent: EvalEnv | null) {
        this.parent = parent;
    }

    getValue(identifier: string): TValue {
        if (identifier in this.valueBindings) {
            return this.valueBindings[identifier];
        } else if (this.parent) {
            return this.parent.getValue(identifier);
        } else throw Error("cannot find value " + identifier);
    }

    getOperator(identifier: string): Operator {
        if (identifier in this.opBindings) {
            return this.opBindings[identifier];
        } else if (this.parent) {
            return this.parent.getOperator(identifier);
        } else throw Error("cannot find operator " + identifier);
    }

    getType(typeExpr: PValue): RType {
        if (this.typeResolver) {
            return this.typeResolver.getType(typeExpr);
        } else if (this.parent) {
            return this.parent.getType(typeExpr);
        } else throw Error("Cannot resolve types")
    }
}

export class StdLibEvalEnv extends BaseEvalEnv {
    constructor (ops: Operator[]) {
        super(null);
        for (const op of ops) {
            if (!op.name) throw Error("Undefined operator");
            this.opBindings[op.name] = op;
        }
    }
}

export class ContractEvalEnv extends BaseEvalEnv {
    constructor (stdlib: EvalEnv, typeResolver: TypeResolver, fieldValues: TValue[], fieldNames: string[]) {
        super(stdlib);
        this.typeResolver = typeResolver;
        for (let i = 0; i < fieldValues.length; ++i) {
            this.valueBindings[fieldNames[i]] = fieldValues[i];
        }
    }
}

export class ActionEvalEnv extends BaseEvalEnv {
    constructor (parent: EvalEnv, paramValues: TValue[], paramNames: string[]) {
        super(parent);
        for (let i = 0; i < paramValues.length; ++i) {
            this.valueBindings[paramNames[i]] = paramValues[i];
        }
    }
}

export const basicOps = [
    OP_EQL, OP_NOT, OP_OR, OP_AND, OP_QUOTE,
    OP_PLUS, OP_MINUS, OP_PUSH,
    OP_EQLS, OP_LESS, OP_LEQ, OP_GREATER, OP_GEQ, OP_IF
];

export const basicStdLib = new StdLibEvalEnv(basicOps);
