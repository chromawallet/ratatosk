/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {CryptoSystem, Signature, PubKey, PrivKey, KeyPair} from './types';

import {createHash, randomBytes} from 'crypto';
import * as secp256k1 from 'secp256k1';

export class ECDSACryptoSystem implements CryptoSystem {
    digest (message: Buffer): Buffer {
        return createHash('sha256').update(message).digest();
    }
    sign (digest: Buffer, keyPair: KeyPair): Signature {
        return secp256k1.sign(digest, keyPair.privKey).signature;
    }
    checkSignature (digest: Buffer, pubKey: PubKey, signature: Signature): boolean {
        return secp256k1.verify(digest, signature, pubKey);
    }

    generatePubKey(privKey: PrivKey): PubKey {
        return secp256k1.publicKeyCreate(privKey);
    }

    deriveSharedKey(pubKey: PubKey, privKey: PrivKey): Buffer {
        return this.digest(secp256k1.ecdh(pubKey, privKey));
    }

    makeKeyPair (entropy?: Buffer):KeyPair {
        let privKey;
        if (entropy) {
            do {
                entropy = this.digest(entropy);
                privKey = entropy;
            } while (!secp256k1.privateKeyVerify(privKey));
        } else {
            do {
                privKey = randomBytes(32);
            } while (!secp256k1.privateKeyVerify(privKey));
        }

        const pubKey = secp256k1.publicKeyCreate(privKey);
        return {pubKey, privKey};
    }

    verifyPrivateKey(privateKey: Buffer): boolean {
        return secp256k1.privateKeyVerify(privateKey);
    }

    verifyPublicKey(publicKey: Buffer): boolean {
        return secp256k1.publicKeyVerify(publicKey);
    }
}