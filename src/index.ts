/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {MyEngine} from './engine';
import {encodeParameters} from './parameters';
import MemoryConsensusEngine from './consensus/memory';
import PostchainGTXConsensusEngine from './consensus/postchain';


// TODO encode, decode stuff is probably temporary

export { MyEngine as DefaultEngine, encodeParameters};
export { GTXMessageCodec } from './gtxmessages';

export const consensus = {
    Memory: MemoryConsensusEngine,
    Postchain: PostchainGTXConsensusEngine
};