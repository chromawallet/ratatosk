/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {
    Cryptor
} from './types';

const crypto = require('crypto');

export function makeAESCryptor(key): Cryptor {
    return {
        encryptor: (bytes) => aesEncrypt(bytes, key),
        decryptor: (bytes) => aesDecrypt(bytes, key)
    }
}

export function aesEncrypt(bytes, key): Buffer {
    const iv = new Buffer(crypto.randomBytes(16));
    const cipher = crypto.createCipheriv('aes-256-gcm', key, iv);
    const enc1 = cipher.update(bytes);
    const enc2 = cipher.final();
    const authTag = cipher.getAuthTag();
    if (authTag.length !== 16) throw Error("Incompatible encryption library");
    return Buffer.concat([iv, authTag, enc1, enc2]);
}

export function aesDecrypt(bytes, key): Buffer {
    const iv = bytes.slice(0, 16);
    const authTag = bytes.slice(16, 32);
    const buff = bytes.slice(32),
        decipher = crypto.createDecipheriv('aes-256-gcm', key, iv);
    decipher.setAuthTag(authTag);
    return Buffer.concat([decipher.update(buff), decipher.final()]);
}
