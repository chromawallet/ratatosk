/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {ParameterInfo} from "./contractdefinition";
import {PValue, TValue, TypeResolver} from "./objects";

export function encodeParameters(params: any, paramInfo: ParameterInfo[], tr: TypeResolver): TValue[] {
    const paramList : TValue[] = [];
    for (const p of paramInfo) {
        if (p.name in params) {
            paramList.push(p.type.fromJSONValue(params[p.name]));
        } else
            throw Error("Missing parameter: " + p.name);
    }
    return paramList;
}