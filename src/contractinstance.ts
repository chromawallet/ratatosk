/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {ContractState} from './contractstate';
import {
    MessageFrame, MessageCodec, RawMessage, ConsensusEngine, MessageResult, LogicalMessage, Cryptor,
    PubKey, Signer
} from "./types";
import {PValue, TValue, TypeResolver} from "./objects";
import {ContractDefinition} from "./contractdefinition";
import {EventEmitter} from "events";
import * as makeConcurrent from 'make-concurrent';

export class ProcessedMessage {
    id: string;
    rawMessage: RawMessage;
    logicalMessage?: LogicalMessage;
    success: boolean;
    beforeState: ContractState;
    afterState?: ContractState;

    constructor (msg: RawMessage, beforeState: ContractState) {
        this.id = msg.getID();
        this.rawMessage = msg;
        this.beforeState = beforeState;
        this.success = false;
    }
}

export default class ContractInstance {
    currentState: ContractState;
    mcodec: MessageCodec;
    lastMessageFrame: MessageFrame;
    ceng: ConsensusEngine;
    chainID: string;
    cryptor: Cryptor;
    contractDefinition: ContractDefinition;
    events : EventEmitter;
    sync: () => Promise<void>;
    messageChain: ProcessedMessage[];
    lastUpdate: number = 0;
    syncError: boolean = false;

    constructor (chainID: string,
                 mcodec: MessageCodec,
                 initMsg: RawMessage,
                 initLM: LogicalMessage,
                 initState: ContractState,
                 ceng: ConsensusEngine,
                 cryptor: Cryptor)
    {
        this.chainID = chainID;
        this.mcodec = mcodec;
        this.currentState = initState;
        this.lastMessageFrame = initMsg;
        this.ceng = ceng;
        this.cryptor = cryptor;
        this.contractDefinition = initState.definition;
        this.events = new EventEmitter();
        const fPM = new ProcessedMessage(initMsg, ContractState.nullState(initState.definition));
        fPM.logicalMessage = initLM;
        fPM.afterState = initState;
        fPM.success = true;
        this.messageChain = [fPM];
        this.sync = makeConcurrent(
            this._sync.bind(this),
            {concurrency: 1}
        );
    }

    protected _applyMessage(msg: RawMessage) {
        if (msg.getPrevID() !== this.lastMessageFrame.getID())
            throw Error("broken message chain");
        const pm = new ProcessedMessage(msg, this.currentState);
        try {
            const lm = this.mcodec.decodeMessage(msg, this.cryptor.decryptor);
            pm.logicalMessage = lm;
            if (lm.action === "$INIT") throw Error("Cannot call $INIT");
            this.currentState = this.currentState.applyAction(lm);
            pm.afterState = this.currentState;
            pm.success = true;
        } catch (e) {
            console.log("Error when decoding message", e.stack || e);
        }
        this.messageChain.push(pm);
        this.lastMessageFrame = msg;
    }

    _sync(): Promise<void> {
        const beforeUpdate = Date.now();
        return this.ceng.getNextMessage(this.chainID, this.lastMessageFrame.getID()).then(
            (msg: MessageResult) => {
                if (msg === null) {
                    // we are up to date.
                    // since we don't know time interval passed since we received message from server,
                    // we set lastUpdate to a momement before update process started, as we are guaranteed
                    // to be past that point.
                    this.lastUpdate = beforeUpdate;
                    this.syncError = false;
                    return;
                }
                if (msg === "error") {
                    //TODO: handle reorganization
                    this.syncError = true;
                    return;
                }
                this._applyMessage(msg);
                return this._sync();
            },
            (err) => {
                this.syncError = true;
                throw err;
            }
        );
    }

    checkAction(action: string, params: TValue[], signedBy: PubKey[]): Error | "ok" {
        try {
            this.currentState.applyAction({
                action, params: params.map( p => p.toPrimitive() ), signedBy
            });
            return "ok";
        } catch (e) {
            return e;
        }
    }

    makeMessage(action: string, params: TValue[], signedBy: PubKey[], extra?: any[]): RawMessage {
        const spec: LogicalMessage = {
            action, params: params.map( p => p.toPrimitive() ), signedBy
        };
        return this.mcodec.encodeMessage(spec,
            this.lastMessageFrame,
            this.cryptor.encryptor,
            extra);
    }

    performAction(action: string, params: TValue[], signedBy: PubKey[], signer: Signer): Promise<ProcessedMessage> {
        const error = this.checkAction(action, params, signedBy);
        if (error !== "ok") {
            return Promise.reject(error);
        }
        const signedMessagePromise = this.mcodec.sign(
            this.makeMessage(action, params, signedBy),
            signedBy,
            signer
        );
        let txid: string = "";
        const currentNumberOfMessages = this.messageChain.length;
        return signedMessagePromise.then( signedMessage => {
            txid = signedMessage.getID();
            return this.ceng.postMessage(this.chainID, signedMessage);
        }).then( () => {
            return this.sync();
        }).then( () => {
            for (let i = currentNumberOfMessages; i < this.messageChain.length; ++i) {
                if (this.messageChain[i].id === txid) {
                    return this.messageChain[i];
                }
            }
            throw Error("Consensus layer error: message was not processed");
        });
    }
}