/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {isBuffer, TValue, RType, ArrayRType, ArrayTValue, NullTValue} from './objects';
import {PubKey, LogicalMessage, ActionMatch} from './types';
import {ContractDefinition, ActionGuard} from './contractdefinition';
import {ContractEvalEnv, basicStdLib, evalExpr, ActionEvalEnv, EvalEnv, toBoolean} from "./expreval";
//import {decodeValue} from "./parameters";
import {flatten} from 'lodash';

function checkGuardClauses(guard: ActionGuard, contractEvalEnv: EvalEnv): boolean {
    for (const clause of guard.clauses) {
        const res = evalExpr(clause, contractEvalEnv);
        if (!toBoolean(res)) return false;
    }
    return true;
}

function pubkeyIntersection(a1: PubKey[], a2: PubKey[]): PubKey[] {
    const intersection: PubKey[] = [];
    for (const e1 of a1) {
        for (const e2 of a2) {
            if (e1.equals(e2)) {
                intersection.push(e1);
                break;
            }
        }
    }
    return intersection;
}

export class ContractState {
    fields: TValue[];
    definition: ContractDefinition;

    constructor(fields: TValue[], definition: ContractDefinition) {
        this.fields = fields;
        this.definition = definition;
    }

    getContractEvalEnv() {
        return new ContractEvalEnv(basicStdLib, this.definition.typeResolver, this.fields, this.definition.fieldNames);
    }

    getFieldValue(fieldName: string, asJSONValue?: boolean): any {
        const fieldIndex = this.definition.fieldIndex[fieldName];
        if (asJSONValue) {
            return this.fields[fieldIndex].toJSONValue();
        } else
            return this.fields[fieldIndex];
    }

    getFieldTypeName(fieldName: string): string {
        const fieldIndex = this.definition.fieldIndex[fieldName];
        if (fieldIndex === undefined) throw Error("Field not found");
        return this.definition.fieldInfo[fieldIndex].type.name;
    }

    getFieldType(fieldName: string): RType {
        const fieldIndex = this.definition.fieldIndex[fieldName];
        if (fieldIndex === undefined) throw Error("Field not found");
        return this.definition.fieldInfo[fieldIndex].type;
    }

    getFieldValues(asJSONValue?: boolean) {
        const res = {};
        for (let i = 0; i < this.fields.length; ++i) {
            let value;
            if (asJSONValue)
                value = this.fields[i].toJSONValue();
            else
                value = this.fields[i];
            res[this.definition.fieldInfo[i].name] = value;
        }
        return res;
    }

    checkSignatures(neededSignatures: PubKey[], signatures: PubKey[]):boolean {
        for (const pk of neededSignatures) {
            if (! signatures.some( elt => pk.equals(elt)))
                return false;
        }
        return true;
    }

    applyAction(m: LogicalMessage): ContractState {
        const action = this.definition.actions[m.action];

        if (!action)
            throw Error("Action not found");

        console.log('action params', action.parameters);

        if (m.params.length !== action.parameters.length)
            throw Error("wrong number of arguments to action");

        const contractEvalEnv = this.getContractEvalEnv();
        if (!checkGuardClauses(action.guard, contractEvalEnv))
            throw Error("guard check failed");
        const signers = this.getActionSigners(m.action);
        if (signers === null) throw Error("action cannot be signed");
        if (!this.checkSignatures(signers, m.signedBy))
            throw Error("guard signature check failed");
        const fields = this.fields.slice();
        const typeResolver = this.definition.typeResolver;

        const params = m.params.map( (p, i) => {
           return action.parameters[i].type.fromPrimitive(p);
        });
        const evalEnv = new ActionEvalEnv(contractEvalEnv,
            params,
            action.parameterNames
        );
        for (const entry of action.update.entries) {
            const fieldIndex = this.definition.fieldIndex[entry.name];
            fields[fieldIndex] = evalExpr(entry.expr, evalEnv);
        }
        return new ContractState(fields, this.definition);
    }

    getActionSigners(actionName: string): PubKey[] | null {
        const action = this.definition.actions[actionName];
        const signers: PubKey[] = [];
        const signerIndex: any = {};
        const addSigner = (pk: PubKey) => {
            const hex = pk.toString("hex");
            if (!(hex in signerIndex)) {
                signerIndex[hex] = true;
                signers.push(pk);
            }
        };

        for (const fieldName of action.guard.signatures) {
            const fieldIndex = this.definition.fieldIndex[fieldName];
            if (fieldIndex === undefined) {
                throw Error("Wrong field reference in action signers");
            } else {
                const value: TValue = this.fields[fieldIndex];
                if (value === null) {
                    if (this.getFieldType(fieldName) instanceof ArrayRType)
                        return null; // action cannot be signed if one of fields is not set yet
                } else {
                    if (value.type.name === "PUBKEY") {
                        addSigner(value.toPrimitive() as Buffer);
                    } else if (value.type instanceof ArrayRType) {
                        for (const val of (value as ArrayTValue).elements) {
                            if (val.type.name !== "PUBKEY") throw Error("Type error in pubkey(s) field");
                            addSigner(val.toPrimitive() as Buffer);
                        }
                    }
                }
            }
        }
        return signers;
    }

    getApplicableActions (pubkeys: PubKey[]):ActionMatch[] {
        const contractEnv = this.getContractEvalEnv();
        const applicableActions: ActionMatch[] = [];
        for (const actionName in this.definition.actions) {
            const action = this.definition.actions[actionName];
            if (checkGuardClauses(action.guard, contractEnv)) {
                let isInvalid = false;
                const actionPubkeys = this.getActionSigners(actionName);
                if (actionPubkeys === null) continue;
                const matchingPubkeys = pubkeyIntersection(actionPubkeys, pubkeys);
                if (matchingPubkeys.length > 0) {
                    applicableActions.push({
                        name: actionName,
                        matchingPubKeys: matchingPubkeys
                    });
                }
            }
        }
        return applicableActions;
    }

    static nullState(def: ContractDefinition) {
        const fields = new Array(def.fieldInfo.length);
        for (let i = 0; i < fields.length; ++i) fields[i] = NullTValue;
        return new ContractState(fields, def);
    }


}