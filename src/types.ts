/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {PValue, TValue} from "./objects";
import {ContractDefinition} from "./contractdefinition";
import ContractInstance from './contractinstance';

export type PubKey = Buffer;
export type PrivKey = Buffer;
export type Signature = Buffer;

// Frame contains message's meta-data & information about the environment
export interface MessageFrame {
    type: string;
    getID(): string;
    getPrevID(): string;
}

// RawMessage is abstract, we only know it's environment
export interface RawMessage extends MessageFrame {}

export interface LogicalMessage {
    action: string;
    params: PValue[];
    signedBy: PubKey[];
}

export interface ConcreteMessage extends LogicalMessage {
    frame: MessageFrame;
}

export type Signer = (digest: Buffer, pubkey: PubKey) => Promise<Signature>;
export type Decryptor = (b: Buffer) => Buffer;
export type Encryptor = Decryptor;

export interface Cryptor {
    decryptor: Decryptor;
    encryptor: Encryptor;
}

export interface  KeyPair {
    pubKey: Buffer;
    privKey: Buffer;
}

export interface CryptoSystem {
    sign (digest: Buffer, keypair: KeyPair): Signature;
    checkSignature (digest: Buffer, pubkey: PubKey, signature: Signature): boolean;
    generatePubKey (privKey: PrivKey): PubKey;
    deriveSharedKey (pubKey: PubKey, privKey: PrivKey): Buffer;
    digest (message: Buffer): Buffer;
    makeKeyPair(entropy?: Buffer): KeyPair;
    verifyPrivateKey(privateKey: Buffer): boolean;
    verifyPublicKey(publicKey: Buffer): boolean;
}

export function makeSimpleSigner(cryptoSystem: CryptoSystem, keyPair: KeyPair): Signer {
    return (digest, pubkey) => {
        if (keyPair.pubKey.equals(pubkey)) {
            return Promise.resolve(cryptoSystem.sign(digest, keyPair));
        } else
            return Promise.reject(Error("Wrong keypair"));
    }
}

export interface  MessageProposal {
    chainID: string;
    rawMessage: RawMessage;
    sign(pubkey: PubKey, signer: Signer): Promise<Buffer>;
    applySignature(signature: Buffer);
    isFullySigned(): boolean;
}

export interface MessageCodec {
    encodeMessageProposal(chainID: string, rm: RawMessage): Buffer;
    decodeMessageProposal(m: Buffer): MessageProposal;

    decodeMessage(rm: RawMessage, decryptor?: Decryptor): LogicalMessage;
    encodeMessage(msg: LogicalMessage, prevMessageFrame: MessageFrame,
                  encryptor?: Encryptor, extra?: any[]): RawMessage;
    sign(rm: RawMessage, pubkeys: PubKey[], signer: Signer): Promise<RawMessage>;
}

export type CryptorGen = (nonce: Buffer) => Cryptor;

export interface Engine extends MessageCodec {
    makeNewContractInstance(def: ContractDefinition, params: TValue[], ce: ConsensusEngine, cryptorGen: Cryptor | CryptorGen): Promise<ContractInstance>;
    loadContractInstance(chainID: string, ce: ConsensusEngine, cryptor: Cryptor): Promise<ContractInstance>;
}

export type MessageResult = RawMessage | null | "error";

export interface ConsensusEngine {
    getNextMessage(chainID: string, msgID: string | null): Promise<MessageResult>;
    // getBlockTimeByTxRID(chainID: string, TxRID: string): Promise<MessageResult>;
    getBlockInfoByMessageID(chainID: string, messageID: string): Promise<any>;
    postMessage(chainID: string | null, m: RawMessage): Promise<void>;
    getInitMessageFrame(): Promise<MessageFrame>;
}

export interface ActionMatch {
    name: string;
    matchingPubKeys: PubKey[];
}
