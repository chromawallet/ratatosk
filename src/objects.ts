/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

export interface PValueArray extends Array<PValue> {}

export interface PValueAssoc {
    [name: string]: PValue;
}

export type PValue = PValueArray | string | number | PValueAssoc | null | Buffer;

export function isArray(v: PValue): v is PValueArray {
    return Array.isArray(v)
}

export function isBuffer(v: PValue): v is Buffer {
    return Buffer.isBuffer(v)
}

export function isAssoc(v: PValue): v is PValueAssoc {
    return (v instanceof Object) && !isArray(v) && !isBuffer(v)
}

export function isString(v: PValue): v is string {
    return typeof v === "string"
}

export function isInteger(v: PValue): v is number {
    return typeof v === "number"
}

export interface TypeResolver {
    getType(typeExpr: PValue): RType;
    getTypeConstructor(name: string): TypeConstructor;
}

export interface TypeConstructor {
   name: string;
   constructType(args: PValue[], res: TypeResolver): RType;
}


export class RecursiveTypeResolver implements TypeResolver {
    types: { [name:string]: RType } = {};
    typeConstructors: { [name:string]: TypeConstructor } = {};
    parent?: TypeResolver;

    constructor(parent?: TypeResolver, types: RType[] = [], typeConstructors: TypeConstructor[] = []) {
        this.parent = parent;
        this.addTypes(types);
        this.addTypeConstructors(typeConstructors);
    }

    addTypes(types: RType[]) {
        for (const type of types) {
            this.types[type.name] = type;
        }
    }

    addTypeConstructors(tcs: TypeConstructor[]) {
        for (const tc of tcs) {
            this.typeConstructors[tc.name] = tc;
        }
    }

    getTypeConstructor(name: string): TypeConstructor {
        if (name in this.typeConstructors)
            return this.typeConstructors[name];
        else if (this.parent)
            return this.parent.getTypeConstructor(name);
        else
            throw Error("Type constructor not found");
    }

    getType(typeSpecifier: PValue): RType {
        if (isString(typeSpecifier)) {
            if (typeSpecifier in this.types)
                return this.types[typeSpecifier];
            else if (this.parent)
                return this.parent.getType(typeSpecifier);
            else throw Error("Type not found:" + typeSpecifier)
        } else if (isArray(typeSpecifier) && typeSpecifier.length > 0) {
            const tcName = typeSpecifier[0];
            if (!isString(tcName)) throw Error("Wrong type specifier");
            const typeConstructor = this.getTypeConstructor(tcName);
            return typeConstructor.constructType(typeSpecifier, this);
        } else
            throw Error("Wrong type specifier");
    }
}

export interface RType {
    isPrimitive: boolean;
    name: string;
    typeSpecifier: PValue;

    fromJSONValue(json: any): TValue;
    fromPrimitive(p: PValue): TValue;
    sameType(t: RType): boolean;
}

export interface TValue {
    type: RType;
    toPrimitive(): PValue;
    toJSONValue(): any;
    equals(other: TValue): boolean;
}

export class PrimitiveRType implements  RType {
    isPrimitive = true;
    typeSpecifier: PValue;
    name: string;
    _JSONPredicate: (p: any) => boolean;
    _toJSON: (p: PValue) => any;
    _fromJSON: (v: any) => PValue;

    constructor(name: string, _JSONPredicate: (p:PValue)=>boolean,
                _toJSON: (p:PValue) => any, _fromJSON: (v:any) => PValue) {
        this.typeSpecifier = name;
        this.name = name;
        this._toJSON = _toJSON;
        this._fromJSON = _fromJSON;
        this._JSONPredicate = _JSONPredicate;
    }

    fromJSONValue(json: any): TValue {
        if (!this._JSONPredicate(json)) throw Error("Incompatible primitive type");
        return this.fromPrimitive(this._fromJSON(json));
    }

    fromPrimitive(p: PValue): TValue {
        return new PrimitiveTValue(this, p);
    }

    sameType(t: RType): boolean {
        return t === this;
    }
}

export class PrimitiveTValue implements TValue {
    type: PrimitiveRType;
    pv: PValue;
    constructor (type: PrimitiveRType, pv: PValue) {
        this.type = type;
        this.pv = pv;
    }

    toPrimitive(): PValue { return this.pv }
    toJSONValue(): any { return this.type._toJSON(this.pv) }

    equals(other: TValue): boolean {
        if (this.type !== other.type) return false;
        const opv = (other as PrimitiveTValue).pv;
        if (this.type.name === "BYTES") {
            return (this.pv as Buffer).equals(opv as Buffer);
        } else {
            return this.pv === opv;
        }
    }
}

const j2pIdentity = (p: PValue) => p;
const p2jIdentity = (v: any) => v;

function isHexString(a: any): boolean {
    return isString(a); //TODO check hex
}


export const PrimitiveTypes = {
    NULL: new PrimitiveRType("NULL", (v) => v === null, p2jIdentity, j2pIdentity),
    BYTES: new PrimitiveRType("BYTES", isHexString,
        (buffer) => (buffer as Buffer).toString('hex'),
        (v) => Buffer.from(v, "hex")
    ),
    STRING: new PrimitiveRType("STRING", isString, p2jIdentity, j2pIdentity),
    INTEGER: new PrimitiveRType("INTEGER", isInteger, p2jIdentity, j2pIdentity),
    BOOLEAN: new PrimitiveRType("BOOLEAN",
        (v) => (typeof v === 'boolean'),
        (v) => (v === 1),
        (v) => (v) ? 1 : 0
    )
};

export const NullTValue = PrimitiveTypes.NULL.fromJSONValue(null);
export const CONST_TRUE = PrimitiveTypes.BOOLEAN.fromJSONValue(true);
export const CONST_FALSE = PrimitiveTypes.BOOLEAN.fromJSONValue(false);

export class ArrayRType implements RType {
    typeSpecifier: PValue;
    elementType: RType;
    isPrimitive = false;
    name: string;

    constructor(elementType: RType) {
        this.elementType = elementType;
        this.typeSpecifier = ["ARRAY", elementType.typeSpecifier];
        this.name = `(ARRAY ${elementType.name})`;
    }

    fromJSONValue(json: any): TValue {
        if (! Array.isArray(json)) throw Error("Array expected");
        return new ArrayTValue(this,
            json.map( elt => this.elementType.fromJSONValue( elt ))
        )
    }
    fromPrimitive(p: PValue): TValue {
        if (! isArray(p)) throw Error("Array expected");
        return new ArrayTValue(this,
            p.map( elt => this.elementType.fromPrimitive( elt ))
        )
    }

    sameType(t: RType): boolean {
        return (this === t || this.name === t.name);
    }
}

export const ArrayTypeConstructor: TypeConstructor = {
    name: "ARRAY",
    constructType(args: PValue[], resolver: TypeResolver): RType {
        if (args.length != 2) throw Error("Invalid arguments to Array type constructor");
        const elementType = resolver.getType(args[1]);
        return new ArrayRType(elementType);
    }
};

export class ArrayTValue implements TValue {
    type: ArrayRType;
    elements: TValue[];

    constructor(type: ArrayRType, elements: TValue[]) {
        this.type = type;
        this.elements = elements;
    }

    toPrimitive(): PValue {
        return this.elements.map( elt => elt.toPrimitive() );
    }

    toJSONValue(): any {
        return this.elements.map( elt => elt.toJSONValue() );
    }

    equals(other: TValue): boolean {
        if (this.type !== other.type) return false;
        const otherArray = other as ArrayTValue;
        if (this.elements.length !== otherArray.elements.length)
            return false;
        for (let i = 0; i < this.elements.length; ++i) {
            if (!this.elements[i].equals(otherArray.elements[i])) return false;
        }
        return true;
    }

    push(elt: TValue): ArrayTValue {
        if (!elt.type.sameType(this.type.elementType)) throw Error("Incompatible element type");
        const newElements = this.elements.slice();
        newElements.push(elt);
        return new ArrayTValue(this.type, newElements);
    }

}


export function boxSimpleValue(p: PValue): TValue {
    if (typeof p === "object") {
        if (isBuffer(p)) return PrimitiveTypes.BYTES.fromPrimitive(p);
        throw Error("PValue isn't simple");
    } else {
        if (p === null) return NullTValue;
        if (isString(p)) return PrimitiveTypes.STRING.fromPrimitive(p);
        if (isInteger(p)) return PrimitiveTypes.INTEGER.fromPrimitive(p);
        throw Error("boxSimpleValue -- really weird error");
    }
}

// type which is like another type but has different name
export class WrappedRType implements  RType {
    typeSpecifier: PValue;
    baseType: RType;
    name: string;
    isPrimitive = false;

    constructor (baseType: RType, name: string) {
        this.typeSpecifier = name;
        this.baseType = baseType;
        this.name = name;
    }

    fromJSONValue(json: any): TValue {
        return new WrappedTValue(this, this.baseType.fromJSONValue(json))
    }

    fromPrimitive(p: PValue): TValue {
        return new WrappedTValue(this, this.baseType.fromPrimitive(p))
    }

    sameType(t: RType): boolean {
        return (this === t) ||
            ((t instanceof WrappedRType)
                && (t.name === this.name)
                && (t.baseType.sameType(this.baseType)));
    }
}

export class WrappedTValue implements  TValue {
    type: WrappedRType;
    wrappedValue: TValue;
    constructor (type: WrappedRType, wrappedValue: TValue) {
        this.type = type;
        this.wrappedValue = wrappedValue;
    }

    toJSONValue(): any { return this.wrappedValue.toJSONValue() }
    toPrimitive(): PValue { return this.wrappedValue.toPrimitive() }

    equals(other: TValue) {
        return (this.type === other.type) &&
            this.wrappedValue.equals((other as WrappedTValue).wrappedValue);
    }
}

export const TypePUBKEY = new WrappedRType(PrimitiveTypes.BYTES, "PUBKEY");

export const BaseTypeResolver = new RecursiveTypeResolver(
    undefined, [
        PrimitiveTypes.BOOLEAN,
        PrimitiveTypes.BYTES,
        PrimitiveTypes.INTEGER,
        PrimitiveTypes.STRING,
        PrimitiveTypes.BYTES,
    ],
    [ ArrayTypeConstructor ]
);

export const StandardTypeResolver = new RecursiveTypeResolver(
    BaseTypeResolver, [
        new WrappedRType(PrimitiveTypes.BYTES, "PUBKEY"),
        new WrappedRType(PrimitiveTypes.BYTES, "CHAINLINK"),
        new WrappedRType(PrimitiveTypes.BYTES, "FILEHASH"),
        new WrappedRType(PrimitiveTypes.BYTES, "FILEDATA")
    ],[]
);