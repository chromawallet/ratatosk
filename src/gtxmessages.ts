/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {gtx} from 'postchain-client';
import {
    RawMessage, Signature, PubKey, Decryptor, LogicalMessage, MessageFrame, MessageCodec, Encryptor,
    Signer, MessageProposal
} from "./types";
import {ECDSACryptoSystem} from "./ecdsa";
import {isBuffer} from "./objects";
import {MyLogicalMessage} from "./messages";

export const cryptoSystem = new ECDSACryptoSystem();

export class GTXNonceMessageFrame implements MessageFrame {
    type = "nonce";
    id: string;
    constructor (nonce: string) {
        this.id = nonce;
    }
    getID(): string { return this.id; }
    getPrevID(): string { return this.id; }
}

export interface GTXOp {
    opName: string;
    args: any[];
}

export type MaybeSignature = Signature | null;

export class GTXGenericMessage {
    digest: Buffer;
    signers: PubKey[];
    blockchainRID: Buffer;
    operations: GTXOp[];
    signatures: MaybeSignature[];
    constructor(gtxObject: any) {
        this.blockchainRID = gtxObject.blockchainRID;
        this.signers = gtxObject.signers;
        this.operations = <GTXOp[]>gtxObject.operations;
        this.signatures = gtxObject.signatures;

        if (!this.signatures || this.signatures.length === 0) {
            this.signatures = this.signers.map(x => null);
        } else {
            if (this.signatures.length !== this.signers.length)
                throw Error("Signatures count mismatch");
        }

        this.digest = cryptoSystem.digest(
          gtx.getBufferToSign(this)
        );
    }

    addSignature(pk: PubKey, signature: Signature) {
        gtx.addSignature(pk, signature, this);
    }

    isFullySigned(verifySignatures: boolean): boolean {
        if (this.signers.length !== this.signatures.length) return false;
        if (this.signatures.indexOf(null) !== -1) return false;
        if (!verifySignatures) return true;
        for (let i = 0; i < this.signatures.length; ++i) {
            if (!cryptoSystem.checkSignature(this.digest, this.signers[i], <Buffer>this.signatures[i]))
                return false;
        }
        return true;
    }

    encode(): Buffer {
        if (this.isFullySigned(false))
            return gtx.serialize(this);
        else
            return gtx.serialize({
                blockchainRID: this.blockchainRID,
                operations: this.operations,
                signers: this.signers,
                signatures: []
            });
    }
}

export class R4GTXRawMessage implements RawMessage {
    type : string = "R4/gtx";
    payload: Buffer;
    id: Buffer;
    prevID: Buffer;
    genericMessage : GTXGenericMessage;
    opIndex : number;
    isFirstMessage: boolean;

    constructor (isFirstMessage: boolean, gm: GTXGenericMessage, index: number, prevID: Buffer, payload: Buffer) {
        this.isFirstMessage = isFirstMessage;
        this.genericMessage = gm;
        this.opIndex = index;
        this.payload = payload;
        this.prevID = prevID;
        this.id = this.computeID();
    }

    static fromGenericMessage (gm: GTXGenericMessage, index: number): R4GTXRawMessage {
        const op = gm.operations[index];
        const createChain: boolean = (op.opName === 'R4createChain');

        if (!createChain && (op.opName !== 'R4postMessage'))
            throw Error("Wrong GTX call");

        const prevID = op.args[0];
        const payload = op.args[1];

        if (!isBuffer(prevID) || !isBuffer(payload)) throw Error("Got wrong types in GM");

        return new this(createChain, gm, index, prevID, payload);
    }

    static makeNew(blockchainRID: Buffer, payload: Buffer, signers: PubKey[], prevMsgFrame: MessageFrame):R4GTXRawMessage {
        let isFirstMessage;
        let opName;
        if (prevMsgFrame.type === "nonce") {
            isFirstMessage = true;
            opName = "R4createChain";
        } else if (prevMsgFrame.type === "R4/gtx") {
            isFirstMessage = false;
            opName = "R4postMessage";
        } else {
            console.log(prevMsgFrame);
            throw Error("R4GTX makeNew unsupported frame type:" + prevMsgFrame.type)
        }
        const prevID = Buffer.from(prevMsgFrame.getID(), 'hex');

        const gm : GTXGenericMessage = new GTXGenericMessage({
            blockchainRID,
            operations: [{
                opName, args: [
                    prevID, payload
                ]
            }],
            signers
        });
        return new this(isFirstMessage, gm, 0, prevID, payload);
    }

    computeID () {
        const allsigners = Buffer.concat(this.genericMessage.signers);
        if (this.isFirstMessage) {
            return cryptoSystem.digest(
                Buffer.concat([this.genericMessage.blockchainRID, this.prevID, this.payload, allsigners])
            );
        } else {
            return cryptoSystem.digest(
                Buffer.concat([this.prevID, this.payload, allsigners])
            );
        }
    }

    getID(): string { return this.id.toString('hex');   }
    getPrevID(): string { return this.prevID.toString('hex'); }

    isFullySigned(verifySignatures: boolean): boolean {
        return this.genericMessage.isFullySigned(verifySignatures);
    }

    getLogicalMessage(decryptor?: Decryptor): LogicalMessage {
        const payload = (decryptor) ? decryptor(this.payload) : this.payload;
        return MyLogicalMessage.decode(payload, this.genericMessage.signers);
    }

    encode() {
        const data = this.genericMessage.encode();
        return data;
    }

    getTxRID() {
        return this.genericMessage.digest;
    }
}

export class GTXMessageProposal implements MessageProposal {
    chainID: string;
    rawMessage: R4GTXRawMessage;

    constructor (chainID: string, rawMessage: R4GTXRawMessage) {
        this.chainID = chainID;
        this.rawMessage = rawMessage;
    }

    sign(pubkey: PubKey, signer: Signer): Promise<Buffer> {
        return signer(this.rawMessage.genericMessage.digest, pubkey).then(signature => {
           return Buffer.concat([pubkey, signature]);
        });
    }
    applySignature(_signature: Buffer) {
        const pubkey = _signature.slice(0, 33);
        const signature = _signature.slice(33);

        this.rawMessage.genericMessage.addSignature(
            pubkey, signature
        );
    }
    isFullySigned(): boolean {
        return this.rawMessage.genericMessage.isFullySigned(true);
    }
}


export class GTXMessageCodec implements MessageCodec {
    blockchainRID: Buffer;

    constructor(blockchainRID: Buffer) {
        this.blockchainRID = blockchainRID;
    }

    encodeMessageProposal(chainID: string, rm: RawMessage): Buffer {
        if (!(rm instanceof R4GTXRawMessage)) throw Error("incompatible message type");
        return gtx.encodeValue([
            Buffer.from(chainID, 'hex'),
            rm.encode()
        ]);
    }

    decodeMessageProposal(m: Buffer): MessageProposal {
        const vals = gtx.decodeValue(m);
        const chainIDBuf = vals[0];
        const gm : GTXGenericMessage = new GTXGenericMessage(gtx.deserialize(vals[1]));
        const rm = R4GTXRawMessage.fromGenericMessage(gm, 0);
        return new GTXMessageProposal(chainIDBuf.toString('hex'), rm);
    }

    decodeMessage(rm: RawMessage, decryptor?: Decryptor): LogicalMessage {
        if (rm.type !== "R4/gtx") throw Error("unrecognized message type");
        return (<R4GTXRawMessage>rm).getLogicalMessage(decryptor);
    }

    encodeMessage(msg: LogicalMessage, prevMessageFrame: MessageFrame, encryptor?: Encryptor, extra?: any[]):RawMessage {
        if (extra !== undefined)  throw Error("no extra options supported");
        let payload =  MyLogicalMessage.from(msg).encode();
        if (encryptor) payload = encryptor(payload);
        return R4GTXRawMessage.makeNew(
            this.blockchainRID,
            payload,
            msg.signedBy,
            prevMessageFrame
        );
    }
    sign(rm: RawMessage, pubkeys: PubKey[], signer: Signer): Promise<RawMessage> {
        if (! (rm instanceof R4GTXRawMessage))  throw Error("incompatible message type");
        const mrm: R4GTXRawMessage = rm;
        const digest = mrm.genericMessage.digest;
        return Promise.all(pubkeys.map( pk => signer(digest, pk))).then( signatures => {
            for (let i = 0; i < signatures.length; i++) {
                mrm.genericMessage.addSignature(
                    pubkeys[i], signatures[i]
                );
            }
            // TODO: check validity
            return mrm
        })
    }
}