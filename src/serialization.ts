/*
 * Copyright (c) 2016-2018 ChromaWay AB. Licensed under the Apache License v. 2.0, see LICENSE
 */

import {define} from 'asn1.js';
import { gtx } from 'postchain-client';
import {PValue} from "./objects";

export function deserializePValue(b: Buffer): PValue {
    return gtx.decodeValue(b);
}

export function serializePValue(v: PValue): Buffer {
    return gtx.encodeValue(v);
}
