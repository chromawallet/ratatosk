# Ratatosk

Ratatosk is the engine of Esplix. It does everything except high-level API.

## Version: 4

This repository contains Ratatosk version 4 and onwards. Older versions are in different repositories.

## Copyright & License information

Copyright (c) 2016-2018 ChromaWay AB. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.

You may find the License in file named LICENSE or [here](http://www.apache.org/licenses/LICENSE-2.0).

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.